import React from 'react'
import AppInfoSection from './Components/LandingPageComponents/AppInfoSection'
import AppsCardBlock from './Components/LandingPageComponents/AppsCardBlock'
import Banner from './Components/LandingPageComponents/Banner'
import CreatorSection from './Components/LandingPageComponents/CreatorSection'
import CreatorSpotlight from './Components/LandingPageComponents/CreatorSpotlight'
import Features from './Components/LandingPageComponents/Features'
import FifthBlackSection from './Components/LandingPageComponents/FifthBlackSection'
import FirstBlackSection from './Components/LandingPageComponents/FirstBlackSection'
import Footer from './Components/LandingPageComponents/Footer'
import FourthBlackSection from './Components/LandingPageComponents/FourthBlackSection'
import NavigationBar from './Components/LandingPageComponents/NavigationBar'
import Pills from './Components/LandingPageComponents/Pills'
import SecondBlackSection from './Components/LandingPageComponents/SecondBlackSection'
import ThirdBlackSection from './Components/LandingPageComponents/ThirdBlackSection'
import TransperencySection from './Components/LandingPageComponents/TransperencySection'
import WorldOfCreator from './Components/LandingPageComponents/WorldOfCreator'

const App = () => {
  return (
    <React.Fragment>
      <NavigationBar />
      <Banner />
      <CreatorSection />
      <FirstBlackSection />
      <Features />
      <SecondBlackSection />
      <AppInfoSection />
      <ThirdBlackSection />
      <AppsCardBlock />
      <FourthBlackSection />
      <TransperencySection />
      <WorldOfCreator />
      <Pills />
      <CreatorSpotlight />
      <FifthBlackSection />
      <Footer />
    </React.Fragment>
  )
}

export default App
