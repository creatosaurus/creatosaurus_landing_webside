import React from 'react'
import '../LandingPageComponentCss/WorldOfCreator.css'

const WorldOfCreator = () => {
    return (
        <div className="world-of-creator">
            <div className="head">
                <h1>Deep dive in the world of creators</h1>
                <button>Visit blog</button>
            </div>
            <div className="blogs">
                <div className="part1">
                    <div className="image-container">
                        <div className="box" />
                        <img src="https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                    </div>
                    <h3>Adopt expert guidance use timely resources and get to know all you</h3>
                    <p>Adopt expert guidance use timely resources and get to know all you Adopt expert guidance use timely resources and get to know all you</p>
                    <span>July 5, 2021</span>
                </div>
                <div className="part2">
                    <div className="card">
                        <div className="info">
                           <p>Adopt expert guidance use timely resources and get to know all you</p>
                           <span>June 5, 2021</span>
                        </div>
                        <img src="https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                    </div>

                    <div className="card">
                        <div className="info">
                           <p>Adopt expert guidance use timely resources and get to know all you</p>
                           <span>June 5, 2021</span>
                        </div>
                        <img src="https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                    </div>

                    <div className="card">
                        <div className="info">
                           <p>Adopt expert guidance use timely resources and get to know all you</p>
                           <span>June 5, 2021</span>
                        </div>
                        <img src="https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WorldOfCreator
