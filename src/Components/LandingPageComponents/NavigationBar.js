import React from 'react'
import '../LandingPageComponentCss/NavigationBar.css'
import Logo from '../../Assets/Logo.svg'

const NavigationBar = () => {
    return (
      <nav>
          <div className="logo-container">
              <img src={Logo} alt="Logo" />
              <span>Creatosaurus</span>
          </div>
          <div className="navigation-section">
              <button>Products</button>
              <button>Resources</button>
              <button>Pricing</button>
          </div>
          <div className="login-links">
              <button>Log in</button>
              <button>Sign up</button>
          </div>
      </nav>
    )
}

export default NavigationBar
