import React from 'react'
import '../LandingPageComponentCss/TransperencySection.css'

const TransperencySection = () => {
    return (
        <div className="transperency-section">
            <h1>Transparency with simplicity</h1>
            <p>Everyone in the team gets a complete view thanks to one unified platform.</p>
            <div className="email-container">
                <input type="text" placeholder="Enter your email" />
                <button>Get Started for Free</button>
            </div>

            <div className="card-container">
                <div className="card-wrapper">
                    <div className="card">
                        <div className="box" />
                    </div>
                    <h4>Collaborate in real time</h4>
                    <p>Everyone in the team gets a complete<br />view thanks to one tool.</p>
                </div>

                <div className="card-wrapper">
                    <div className="card">
                        <div className="box box2" />
                    </div>
                    <h4>Invite and share</h4>
                    <p>Everyone in the team gets a complete<br />view thanks to one tool.</p>
                </div>

                <div className="card-wrapper">
                    <div className="card">
                        <div className="box box3" />
                    </div>
                    <h4>Deep Integrations</h4>
                    <p>Everyone in the team gets a complete<br />view thanks to one tool.</p>
                </div>

                <div className="card-wrapper">
                    <div className="card">
                        <div className="box box4" />
                    </div>
                    <h4>Connect your favourite tools</h4>
                    <p>Everyone in the team gets a complete<br />view thanks to one tool.</p>
                </div>
            </div>
        </div>
    )
}

export default TransperencySection
