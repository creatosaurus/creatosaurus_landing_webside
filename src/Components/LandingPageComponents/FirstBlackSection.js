import React from 'react'
import '../LandingPageComponentCss/FirstBlackSection.css'

const FirstBlackSection = () => {
    return (
        <div className="first-black-section">
            <p>We are in the midst<br />of a new creator age.</p>
            <p>Imagine a world where chaos is <br /> replaced with free time, <br />creative work and stories.</p>
            <p>Creatosaurus is a platform for storytelling.</p>
            <p>Built for companies with distributed teams <br /> and global creative individuals.</p>
        </div>
    )
}

export default FirstBlackSection
