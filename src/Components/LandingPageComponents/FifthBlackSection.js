import React from 'react'
import '../LandingPageComponentCss/FifthBlackSection.css'

const FifthBlackSection = () => {
    return (
        <div className="fifth-black-section">
             <h1>You focus on telling stories,<br />we do everything else.</h1>
             <button>Signup for Free</button>
        </div>
    )
}

export default FifthBlackSection
