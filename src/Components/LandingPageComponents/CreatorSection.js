import React from 'react'
import '../LandingPageComponentCss/CreatorSection.css'

const CreatorSection = () => {
    return (
        <section className="creator-section">
            <div className="brands">
                <h5>Trusted by the Best</h5>
                <div>
                    <span>Creators</span>
                    <span>Creators</span>
                    <span>Creators</span>
                    <span>Creators</span>
                </div>
            </div>
            <div className="info-container">
                <img src="https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="" />
                <div className="info">
                    <span>🕺🏻</span>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <span className="name">Raj Sharma</span>
                    <span className="designation">Founder, Abc Music</span>
                </div>
            </div>
            <div className="button-container">
                <button>🕺🏻 &nbsp;&nbsp;&nbsp;Creators</button>
                <button>🎸 &nbsp;&nbsp;&nbsp;Community Builders</button>
                <button>🚀 &nbsp;&nbsp;&nbsp;Startups</button>
                <button>🎯 &nbsp;&nbsp;&nbsp;Marketers</button>
                <button>💃🏻 &nbsp;&nbsp;&nbsp;Influencers</button>
                <button>💈 &nbsp;&nbsp;&nbsp;Social Media Mangers</button>
            </div>
        </section>
    )
}

export default CreatorSection
