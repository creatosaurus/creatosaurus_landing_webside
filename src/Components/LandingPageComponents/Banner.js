import React from 'react'
import '../LandingPageComponentCss/Banner.css'
import BannerImage from '../../Assets/Banner.svg'
import People1 from '../../Assets/BannerPeople1.svg'
import People2 from '../../Assets/BannerPeople2.svg'
import People3 from '../../Assets/BannerPeople3.svg'
import People4 from '../../Assets/BannerPeople4.svg'

const Banner = () => {
    return (
        <header>
            <div className="heading-section">
                <h1>Workflow Optimization for Creators</h1>
                <p>Curate ideas, design graphics, edit videos, schedule posts,<br />manage your social media accounts and more… under one true home for creators</p>
                <div>
                    <input type="text" placeholder="Enter your email" />
                    <button>Get Started for Free</button>
                </div>
                <span>“Amazingly beautiful” - Raj Sharma</span>
            </div>
            <div className="banner-section">
                <img className="banner" src={BannerImage} alt="" />
                <img className="img1" src={People1} alt="" />
                <img className="img2" src={People2} alt="" />
                <img className="img3" src={People3} alt="" />
                <img className="img4" src={People4} alt="" />
            </div>
        </header>
    )
}

export default Banner
