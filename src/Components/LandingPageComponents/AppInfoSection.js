import React from 'react'
import '../LandingPageComponentCss/AppInfoSection.css'

const AppInfoSection = () => {
    return (
        <div className="app-info-section">
            <div className="card">
                <div className="info">
                    <h5>AI powered copywriting <br />that scales</h5>
                    <p>Create marketing copies at scale <br />with our AI powered content generation</p>
                    <div className="tags-container">
                        <button><span>Facebook Ads</span></button>
                        <button><span>Blog Titles</span></button>
                    </div>
                    <div className="tags-container">
                        <button><span>Google Ads</span></button>
                        <button><span>Instagram Caption</span></button>
                    </div>
                    <div className="tags-container">
                        <button><span>Product Description</span></button>
                        <button><span>Facebook Ads</span></button>
                    </div>
                    <div className="quote">“Create marketing copies at scale <br />with our AI powered content generation”</div>
                    <div className="profile">
                        <div className="profile-image" />
                        <div className="profile-info">
                            <span>Raj Sharma</span>
                            <span>Founder, Abc Music</span>
                        </div>
                    </div>
                </div>
                <div className="show-case">
                    <div className="box" />
                </div>
            </div>

            <div className="card">
                <div className="show-case">
                    <div className="box box2" />
                </div>
                <div className="info">
                    <h5>Create beautiful graphics<br />for your social media</h5>
                    <p>Create marketing copies at scale <br />with our AI powered content generation</p>
                    <div className="tags-container tags-container1">
                        <button><span>Drag & Drop</span></button>
                        <button><span>Free Stock Images</span></button>
                    </div>
                    <div className="tags-container tags-container1">
                        <button><span>Template Based</span></button>
                        <button><span>Free Fonts</span></button>
                    </div>
                    <div className="tags-container tags-container1">
                        <button><span>Multi Language</span></button>
                        <button><span>Illustrations & SVGs</span></button>
                    </div>
                    <div className="quote">“Create marketing copies at scale <br />with our AI powered content generation”</div>
                    <div className="profile">
                        <div className="profile-image" />
                        <div className="profile-info">
                            <span>Raj Sharma</span>
                            <span>Founder, Abc Music</span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="card">
                <div className="info">
                    <h5>AI powered copywriting <br />that scales</h5>
                    <p>Create marketing copies at scale <br />with our AI powered content generation</p>
                    <div className="tags-container tags-container2">
                        <button><span>Facebook Ads</span></button>
                        <button><span>Blog Titles</span></button>
                    </div>
                    <div className="tags-container tags-container2">
                        <button><span>Google Ads</span></button>
                        <button><span>Instagram Caption</span></button>
                    </div>
                    <div className="tags-container tags-container2">
                        <button><span>Product Description</span></button>
                        <button><span>Facebook Ads</span></button>
                    </div>
                    <div className="quote">“Create marketing copies at scale <br />with our AI powered content generation”</div>
                    <div className="profile">
                        <div className="profile-image" />
                        <div className="profile-info">
                            <span>Raj Sharma</span>
                            <span>Founder, Abc Music</span>
                        </div>
                    </div>
                </div>
                <div className="show-case">
                    <div className="box box3" />
                </div>
            </div>

            <div className="card">
                <div className="show-case">
                    <div className="box box4" />
                </div>
                <div className="info">
                    <h5>Create beautiful graphics<br />for your social media</h5>
                    <p>Create marketing copies at scale <br />with our AI powered content generation</p>
                    <div className="tags-container tags-container3">
                        <button><span>Drag & Drop</span></button>
                        <button><span>Free Stock Images</span></button>
                    </div>
                    <div className="tags-container tags-container3">
                        <button><span>Template Based</span></button>
                        <button><span>Free Fonts</span></button>
                    </div>
                    <div className="tags-container tags-container3">
                        <button><span>Multi Language</span></button>
                        <button><span>Illustrations & SVGs</span></button>
                    </div>
                    <div className="quote">“Create marketing copies at scale <br />with our AI powered content generation”</div>
                    <div className="profile">
                        <div className="profile-image" />
                        <div className="profile-info">
                            <span>Raj Sharma</span>
                            <span>Founder, Abc Music</span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="card">
                <div className="info">
                    <h5>AI powered copywriting <br />that scales</h5>
                    <p>Create marketing copies at scale <br />with our AI powered content generation</p>
                    <div className="tags-container tags-container4">
                        <button><span>Facebook Ads</span></button>
                        <button><span>Blog Titles</span></button>
                    </div>
                    <div className="tags-container tags-container4">
                        <button><span>Google Ads</span></button>
                        <button><span>Instagram Caption</span></button>
                    </div>
                    <div className="tags-container tags-container4">
                        <button><span>Product Description</span></button>
                        <button><span>Facebook Ads</span></button>
                    </div>
                    <div className="quote">“Create marketing copies at scale <br />with our AI powered content generation”</div>
                    <div className="profile">
                        <div className="profile-image" />
                        <div className="profile-info">
                            <span>Raj Sharma</span>
                            <span>Founder, Abc Music</span>
                        </div>
                    </div>
                </div>
                <div className="show-case">
                    <div className="box box5" />
                </div>
            </div>
        </div>
    )
}

export default AppInfoSection
