import React from 'react'
import '../LandingPageComponentCss/Footer.css'
import Logo from '../../Assets/Logo.svg'
import Twitter from '../../Assets/Twitter.svg'
import Instagram from '../../Assets/Instagram.svg'
import Facebook from '../../Assets/Facebook.svg'
import LinkedIn from '../../Assets/LinkedIn.svg'
import YouTube from '../../Assets/YouTube.svg'

const Footer = () => {
    return (
        <footer>
            <div className="footer-top">
                <div className="part1">
                    <div className="title">
                        <img src={Logo} alt="" />
                        <span>Creatosaurus</span>
                    </div>
                    <p>All in one workflow optimization<br />platform for creators.</p>
                    <div className="social-media">
                        <img src={Twitter} alt="" />
                        <img src={Instagram} alt="" />
                        <img src={Facebook} alt="" />
                        <img src={LinkedIn} alt="" />
                        <img src={YouTube} alt="" />
                    </div>
                </div>

                <div className="part2">
                    <span>Company</span>
                    <span>About</span>
                    <span>Careers</span>
                    <span>Contact</span>
                </div>

                <div className="part3">
                    <span>Products</span>
                    <span>Features</span>
                    <span>Status</span>
                    <span>Pricing</span>
                    <span>Roadmap</span>
                </div>

                <div className="part4">
                    <span>Resources</span>
                    <span>Blogs</span>
                    <span>FAQ</span>
                    <span>Join Discord Server</span>
                    <span>Knowledge Base</span>
                    <span>Tweet at us</span>
                </div>
            </div>

            <div className="footer-bottom">
                 <div className="border" />
                 <div className="terms-container">
                     <div className="terms-wrapper">
                         <span>© 2021 Creatosaurus. All rights reserved.</span>
                         <span>Terms of Service</span>
                         <span>Privacy Policy</span>
                     </div>
                     <span style={{fontSize:14}}>Made with ❤️ for Creators</span>
                 </div>
            </div>
        </footer>
    )
}

export default Footer
