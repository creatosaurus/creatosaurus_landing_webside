import React from 'react'
import '../LandingPageComponentCss/Pills.css'

const Pills = () => {
    return (
        <div className="pills-container">
            <h1>All we are offering is the truth, nothing more.</h1>
            <p>This is your last chance. After this, there is no turning back.</p>
            <div className="card-container">
                <div className="box">
                    <span>I’ll take the red pill.</span>
                </div>
                <div className="box box1">
                    <span>I’ll take the blue pill.</span>
                </div>
            </div>
        </div>
    )
}

export default Pills
