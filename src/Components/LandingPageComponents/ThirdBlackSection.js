import React from 'react'
import '../LandingPageComponentCss/ThirdBlackSection.css'

const ThirdBlackSection = () => {
    return (
        <div className="third-black-section">
            <h4>All in One Creator Stack</h4>
            <p>Tired of juggling between multiple tools and integrations? Streamline & optimize your entire process.</p>
            <div className="card-container">
                <div className="card">
                    <div className="box">
                        <span className="title">Curate</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>

                <div className="card card2">
                    <div className="box">
                        <span className="title1">Create</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>

                <div className="card card3">
                    <div className="box">
                        <span className="title2">Distribute</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>
            </div>

            <div className="card-container">
                <div className="card card4">
                    <div className="box">
                        <span className="title3">Collaborate</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>


                <div className="card card5">
                    <div className="box">
                        <span className="title4">Manage</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>
            </div>

            <div className="card-container">
                <div className="card card6">
                    <div className="box">
                        <span className="title5">Analyse</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>

                <div className="card card7">
                    <div className="box">
                        <span className="title6">Advertise</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>

                <div className="card card8">
                    <div className="box">
                        <span className="title7">Monetize</span>
                        <p>Create marketing copies at <br /> scale with our AI powered</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ThirdBlackSection
