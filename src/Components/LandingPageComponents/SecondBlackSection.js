import React from 'react'
import '../LandingPageComponentCss/SecondBlackSection.css'

const SecondBlackSection = () => {
    return (
        <div className="second-black-section">
            <p>As makers, creators, movers and shakers,<br />we are in this together.</p>
            <p>We have been building <br />something new for creators like us.</p>
            <p>All in one creator stack.<br />That's right, we are making it.</p>
            <p>While it's a crazy idea and<br />has never been tried before,<br />we are up for the challenge.</p>
        </div>
    )
}

export default SecondBlackSection
